#ifndef RING_BUFFER_H
#define RING_BUFFER_H

#include <algorithm>
#include <vector>

namespace Utils
{
	//Forward declaration
	template <typename T> class CRingBuffer;

	/**
	 * An iterator to the ring buffer.
	 * This type of iterator prohibits changing elements of the ring buffer.
	 */
	template <typename T>
	class CRingBufferConstIterator
	{
	public:
		typedef CRingBufferConstIterator<T> Type;
		typedef CRingBuffer<T> BufferType;

		/**
		 * Constructor.
		 * @param (IN) rBuffer - a reference to a ring buffer the iterator is related to.
		 * @param (IN) nIndex - an index of the item the iterator will point at.
		 */
		CRingBufferConstIterator(const BufferType& rBuffer, int nIndex)
		: m_pBuffer(&rBuffer)
		, m_nIndex(nIndex)
		, m_bSet(true)
		{}

		/**
		 * Overloaded dereferencing operator ->.
		 * @return a pointer to the item the iterator relates to.
		 */
		const T* operator->() const
		{
			return &m_pBuffer->m_Buffer[m_nIndex];
		}

		/**
		 * Overloaded dereferencing operator *.
		 * @return a reference to the item the iterator relates to.
		 */
		const T& operator*() const
		{
			return m_pBuffer->m_Buffer[m_nIndex];
		} 

		/**
		 * Overloaded pre-increment operator.
		 * @return a reference to the item the iterator relates to after the incrementing.
		 */
		const Type& operator++()
		{
			if (m_nIndex == m_pBuffer->m_Buffer.size() - 1)
			{
				m_nIndex = 0;
			}
			else
			{
				++m_nIndex;
			}
			return *this;
		}

		/**
		 * Overloaded post-increment operator.
		 * @return a copy of the item the iterator relates to.
		 */
		Type operator++(int)
		{
			Type tmp(*this);
			operator++();
			return tmp;
		}

		/**
		 * Overloaded + operator.
		 * @param (IN) nIncrement - a number by which a copy of the iterator is to be incremented.
		 * @return an incremented copy of the iterator.
		 */
		Type operator+(int nIncrement) const
		{
			Type result(*this);
			result += nIncrement;
			return result;
		}

		/**
		 * Overloaded += operator.
		 * @param (IN) nIncrement - a number by which the iterator is to be incremented.
		 * @return a reference to the iterator.
		 */
		Type& operator+=(int nIncrement)
		{
			if (nIncrement >= 0)
			{
				m_nIndex = (m_nIndex + nIncrement) % m_pBuffer->GetSize();
			}
			else
			{
				m_nIndex = m_pBuffer->GetSize() + nIncrement % m_pBuffer->GetSize();
			}
			return *this;
		}

		/** Resets the iterator. It makes IsSet() return false. */
		void Reset()
		{
			m_bSet = false;
		}

		/** Determines whether the iterator is set */
		bool IsSet() const
		{
			return m_bSet;
		}

	protected:
		/** A ring buffer the iterator is related to */
		const BufferType* m_pBuffer;
	
		/** An index of the item the iterator relates to */
		int m_nIndex;

		/** A flag that helps to determine whether the iterator is set */
		bool m_bSet;

	private:
		template <typename Q> friend class CRingBuffer;
	};

	/** An iterator to the ring buffer */
	template <typename T>
	class CRingBufferIterator
	{
	public:
		typedef CRingBuffer<T> BufferType;
		typedef CRingBufferIterator<T> Type;

		/**
		 * Constructor.
		 * @param (IN) pBuffer - a pointer to a ring buffer the iterator is related to.
		 * @param (IN) nIndex - an index of the item the iterator will point at.
		 */
		CRingBufferIterator(BufferType& rBuffer, int nIndex)
		: m_ConstIterator(rBuffer, nIndex)
		{}

		/** Converts to BufferType::ConstIterator. */
		operator typename BufferType::ConstIterator() { return m_ConstIterator; }

		/**
		 * Overloaded dereferencing operator ->.
		 * @return a pointer to the item the iterator relates to.
		 */
		T* operator->()
		{
			return const_cast<T*>(m_ConstIterator.operator->());
		}

		/**
		 * Overloaded dereferencing operator *.
		 * @return a reference to the item the iterator relates to.
		 */
		T& operator*()
		{
			return const_cast<T&>(m_ConstIterator.operator*());
		}

		/**
		 * Overloaded pre-increment operator.
		 * @return a reference to the item the iterator relates to after the incrementing.
		 */
		Type& operator++()
		{
			m_ConstIterator.operator++();
			return *this;
		}

		/**
		 * Overloaded post-increment operator.
		 * @return a copy of the item the iterator relates to.
		 */
		Type operator++(int)
		{
			Type tmp(*this);
			m_ConstIterator.operator++();
			return tmp;
		}

		/**
		 * Overloaded + operator.
		 * @param (IN) nIncrement - a number by which a copy of the iterator is to be incremented.
		 * @return an incremented copy of the iterator.
		 */
		Type operator+(int nIncrement) const
		{
			Type result(*this);
			result.m_ConstIterator = m_ConstIterator + nIncrement;
			return result;
		}

		/**
		 * Overloaded += operator.
		 * @param (IN) nIncrement - a number by which the iterator is to be incremented.
		 * @return a reference to the iterator.
		 */
		Type& operator+=(int nIncrement)
		{
			m_ConstIterator += nIncrement;
			return *this;
		}

		/** Resets the iterator. It makes IsSet() return false. */
		void Reset()
		{
			m_ConstIterator.Reset();
		}

		/** Determines whether the iterator is set */
		bool IsSet() const
		{
			return m_ConstIterator.IsSet();
		}

	private:
		typename BufferType::ConstIterator m_ConstIterator;

		template <typename Q> friend class CRingBuffer;
	};

	/** Ring buffer. */
	template <typename T>
	class CRingBuffer
	{
	public:
		typedef CRingBuffer<T> Type;
		typedef CRingBufferConstIterator<T> ConstIterator;
		typedef CRingBufferIterator<T> Iterator;

		/**
		 * Constructor.
		 * @param (IN) nSize - the size of the buffer.
		 * @param (IN) rInitializerObj - an object to be used to initialize all items of the buffer.
		 */
		CRingBuffer(int nSize, const T& rInitializerObj): m_Buffer(nSize)
		{
			for (int i = 0; i < nSize; ++i)
			{
				m_Buffer[i] = rInitializerObj;
			}
		}
	
		/**
		 * Returns ConstIterator to the first element.
		 * @return ConstIterator to the first element.
		 */
		ConstIterator GetConstIterator() const { return ConstIterator(*this, 0); }

		/**
		 * Returns an iterator to the first element.
		 * @return an iterator to the first element.
		 */
		Iterator GetIterator() { return Iterator(*this, 0); }

		/**
		 * Returns the size of the buffer.
		 * @return the size of the buffer.
		 */
		int GetSize() const { return m_Buffer.size(); }

		/**
		 * Returns the first element of the buffer for which predicate \p pred returns true.
		 * @param (IN) - unary predicate which returns true for the required element.
		 * @return the first element of the buffer for which predicate \p pred returns true.
		 */
		template <typename UnaryPredicate>
		ConstIterator FindIf(UnaryPredicate pred) const
		{
			ConstIterator result(*this, 0);
			typename BufferType::const_iterator it = std::find_if(m_Buffer.begin(), m_Buffer.end(), pred);
			
			if (it == m_Buffer.end())
			{
				result.Reset();
			}
			else
			{
				result += std::distance(m_Buffer.begin(), it);
			}
			return result;
		}

		/**
		 * Returns the first element of the buffer for which predicate \p pred returns true.
		 * @param (IN) - unary predicate which returns true for the required element.
		 * @return the first element of the buffer for which predicate \p pred returns true.
		 */
		template <typename UnaryPredicate>
		Iterator FindIf(UnaryPredicate pred)
		{
			Iterator result(*this, 0);
			const Type* pConstThis = this;
			ConstIterator iter = pConstThis->FindIf(pred);

			if (iter.IsSet())
			{
				result.m_ConstIterator.m_nIndex = iter.m_nIndex;
			}
			else
			{
				result.Reset();
			}			
			return result;
		}

		/**
		 * Resets all elements with the specified item.
		 * @param (IN) rItem - an item to be assigned to all elements.
		 */
		void Reset(const T& rItem)
		{
			std::fill(m_Buffer.begin(), m_Buffer.end(), rItem);
		}

	protected:
		typedef std::vector<T> BufferType;

		BufferType m_Buffer;

	private:
		template <typename Q> friend class CRingBufferConstIterator;
	};

} //namespace Utils

#endif //RING_BUFFER_H
