#include "Precompiled.h"
#include "RingBufferTestSuite.h"

#include "../RingBuffer.h"

CPPUNIT_TEST_SUITE_REGISTRATION(CRingBufferTestSuite);

using namespace Utils;

namespace
{
	struct CItem
	{
		CItem(): m_nValue(-1) {}

		CItem(int nValue): m_nValue(nValue) {}

		int m_nValue;
	};

	typedef CRingBuffer<int> CIntRingBuffer;
	typedef CRingBuffer<CItem> CItemRingBuffer;
} //anonymous namespace

//////////////////////////////////////////////////////////////////////////

void CRingBufferTestSuite::TestSize()
{
	CRingBuffer<int> ring(3, 0);
	CPPUNIT_ASSERT_EQUAL(3, ring.GetSize());
}

//////////////////////////////////////////////////////////////////////////

void CRingBufferTestSuite::TestInit()
{
	const int nSize = 3;
	const int nInitValue = 7;
	CIntRingBuffer ring(nSize, nInitValue);
	CIntRingBuffer::ConstIterator it = ring.GetConstIterator();

	for (int i = 0; i < nSize; ++i)
	{
		const int nValue = *it;
		CPPUNIT_ASSERT_EQUAL(nInitValue, nValue);
		++it;
	}
}

//////////////////////////////////////////////////////////////////////////

void CRingBufferTestSuite::TestIterators()
{
	const int nSize = 3;
	const int nInitValue = 7;
	CIntRingBuffer ring(nSize, nInitValue);

	//test init values
	{
		CIntRingBuffer::Iterator it = ring.GetIterator();

		for (int i = 0; i < nSize; ++i)
		{
			int nValue = *it;
			CPPUNIT_ASSERT_EQUAL(nInitValue, nValue);		
			++it;
		}
	}

	//Change the values of the buffer to test if the iterating works properly
	int nNewValue = 0;
	{
		CIntRingBuffer::Iterator it = ring.GetIterator();
	
		for (int i = 0; i < nSize; ++i)
		{
			int& rValue = *it;
			rValue = ++nNewValue;
			++it;
		}
	}

	nNewValue = 0;
	{
		CIntRingBuffer::Iterator it = ring.GetIterator();

		//Test the iterating over the new values
		for (int i = 0; i < nSize; ++i)
		{
			int nValue = *it;
			++nNewValue;
			CPPUNIT_ASSERT_EQUAL(nNewValue, nValue);
			++it;
		}
	}

	nNewValue = 0;
	{
		CIntRingBuffer::ConstIterator it = ring.GetConstIterator();
		//Test ConstIterator for the new values
		for (int i = 0; i < nSize; ++i)
		{
			int nValue = *it;
			++nNewValue;
			CPPUNIT_ASSERT_EQUAL(nNewValue, nValue);
			++it;
		}
	}
}

//////////////////////////////////////////////////////////////////////////

void CRingBufferTestSuite::TestConstDereferencing()
{
	int nExpected = -1;
	CItemRingBuffer ring(1, CItem(nExpected));
	int nValue = ring.GetConstIterator()->m_nValue;
	CPPUNIT_ASSERT_EQUAL(nExpected, nValue);

	const CItem& rItem = *ring.GetConstIterator();
	nValue = rItem.m_nValue;
	CPPUNIT_ASSERT_EQUAL(nExpected, nValue);
}

//////////////////////////////////////////////////////////////////////////

void CRingBufferTestSuite::TestDereferencing()
{
	int nExpected = -1;
	CItemRingBuffer ring(1, CItem(nExpected));
	int nValue = ring.GetIterator()->m_nValue;
	CPPUNIT_ASSERT_EQUAL(nExpected, nValue);

	CItem& rItem = *ring.GetIterator();
	nExpected = 777;
	rItem.m_nValue = nExpected;
	nValue = ring.GetIterator()->m_nValue;
	CPPUNIT_ASSERT_EQUAL(nExpected, nValue);

	nExpected = 88;
	ring.GetIterator()->m_nValue = nExpected;
	rItem = *ring.GetIterator();
	nValue = rItem.m_nValue;
	CPPUNIT_ASSERT_EQUAL(nExpected, nValue);
}

//////////////////////////////////////////////////////////////////////////

void CRingBufferTestSuite::TestOverwriting()
{
	CIntRingBuffer ring(3, 0);
	CIntRingBuffer::Iterator it = ring.GetIterator();

	for (int i = 0; i < 6; ++i)
	{
		*it = i;
		++it;
	}

	for (int i = 0; i < 3; ++i)
	{
		const int nValue = *it;
		const int nExpected = 3 + i;
		CPPUNIT_ASSERT_EQUAL(nExpected, nValue);
		++it;
	}
}

//////////////////////////////////////////////////////////////////////////

void CRingBufferTestSuite::TestCopying()
{
	CIntRingBuffer ring(3, 0);
	CIntRingBuffer::Iterator it = ring.GetIterator();

	for (int i = 0; i < 3; ++i)
	{
		*it = i;
		++it;
	}

	CIntRingBuffer::Iterator it2 = it;

	for (int i = 0; i < 3; ++i)
	{
		int nValue = *it2;
		CPPUNIT_ASSERT_EQUAL(i, nValue);
		++it2;
	}

	for (int i = 0; i < 3; ++i)
	{
		int nValue = *it;
		CPPUNIT_ASSERT_EQUAL(i, nValue);
		++it;
	}
}

//////////////////////////////////////////////////////////////////////////

void CRingBufferTestSuite::TestPostIncrement()
{
	CIntRingBuffer ring(3, 0);
	CIntRingBuffer::Iterator it = ring.GetIterator();

	for (int i = 0; i < 3; ++i)
	{
		*it = i;
		++it;
	}

	{
		CIntRingBuffer::Iterator it2 = it++;
		int nValue = *it2;
		CPPUNIT_ASSERT_EQUAL(0, nValue);
		nValue = *it;
		CPPUNIT_ASSERT_EQUAL(1, nValue);
	}
	{
		it = ring.GetIterator();
		CIntRingBuffer::ConstIterator it2 = it++;
		int nValue = *it2;
		CPPUNIT_ASSERT_EQUAL(0, nValue);
		++it2;
		nValue = *it2;
		CPPUNIT_ASSERT_EQUAL(1, nValue);
		nValue = *it;
		CPPUNIT_ASSERT_EQUAL(1, nValue);
	}
}

//////////////////////////////////////////////////////////////////////////

void CRingBufferTestSuite::TestIsSet()
{
	CIntRingBuffer ring(1, 0);

	//Test Iterator
	{
		CIntRingBuffer::Iterator it = ring.GetIterator();
		bool bSet = it.IsSet();
		CPPUNIT_ASSERT_EQUAL(true, bSet);
		it.Reset();
		bSet = it.IsSet();
		CPPUNIT_ASSERT_EQUAL(false, bSet);
	}	

	//Test ConstIterator
	{
		CIntRingBuffer::ConstIterator it = ring.GetIterator();
		bool bSet = it.IsSet();
		CPPUNIT_ASSERT_EQUAL(true, bSet);
		it.Reset();
		bSet = it.IsSet();
		CPPUNIT_ASSERT_EQUAL(false, bSet);
	}
}

//////////////////////////////////////////////////////////////////////////

void CRingBufferTestSuite::TestPlus()
{
	CIntRingBuffer ring(3, 0);
	CIntRingBuffer::Iterator it = ring.GetIterator();

	for (int i = 0; i < 3; ++i)
	{
		*it = i;
		++it;
	}

	//Test ConstIterator
	{
		CIntRingBuffer::ConstIterator itConst = it;

		for (int i = 0; i < 20; ++i)
		{
			CIntRingBuffer::ConstIterator itConst2 = itConst + i;
			int nValue = *itConst2;
			CPPUNIT_ASSERT_EQUAL(i % 3, nValue);

			//test if itConst is not affected
			nValue = *itConst;
			CPPUNIT_ASSERT_EQUAL(0, nValue);

			//test if 'it' is not affected
			nValue = *it;
			CPPUNIT_ASSERT_EQUAL(0, nValue);
		}

		//Test the + operator with a negative number
		{
			itConst = ring.GetConstIterator() + (-4);
			int nValue = *itConst;
			CPPUNIT_ASSERT_EQUAL(2, nValue);
		}		
	}

	//Test Iterator
	{
		for (int i = 0; i < 20; ++i)
		{
			CIntRingBuffer::Iterator it2 = it + i;
			int nValue = *it2;
			CPPUNIT_ASSERT_EQUAL(i % 3, nValue);

			//test if 'it' is not affected
			nValue = *it;
			CPPUNIT_ASSERT_EQUAL(0, nValue);
		}

		//Test the + operator with a negative number
		{
			it = ring.GetIterator() + (-4);
			int nValue = *it;
			CPPUNIT_ASSERT_EQUAL(2, nValue);
		}	
	}
}

//////////////////////////////////////////////////////////////////////////

void CRingBufferTestSuite::TestPlus2()
{
	CIntRingBuffer ring(3, 0);
	CIntRingBuffer::Iterator it = ring.GetIterator();

	for (int i = 0; i < 3; ++i)
	{
		*it = i;
		++it;
	}

	//Test ConstIterator
	{
		CIntRingBuffer::ConstIterator itConst = ring.GetConstIterator();
		itConst += 2;
		int nValue = *itConst;
		CPPUNIT_ASSERT_EQUAL(2, nValue);
		itConst += 2;
		nValue = *itConst;
		CPPUNIT_ASSERT_EQUAL(1, nValue);
	}

	//Test Iterator
	{
		CIntRingBuffer::Iterator it2 = ring.GetIterator();
		it2 += 2;
		int nValue = *it2;
		CPPUNIT_ASSERT_EQUAL(2, nValue);
		it2 += 2;
		nValue = *it2;
		CPPUNIT_ASSERT_EQUAL(1, nValue);
	}
}

//////////////////////////////////////////////////////////////////////////

void CRingBufferTestSuite::TestFindIf()
{
	CItemRingBuffer ring(3, CItem(-1));
	CItemRingBuffer::Iterator it = ring.GetIterator();

	for (int i = 0; i < 3; ++i)
	{
		it->m_nValue = i;
		++it;
	}

	class IsEqual
	{
	public:
		IsEqual(int nValue)
		: m_nValue(nValue)
		{}

		bool operator()(const CItem& rItem)
		{
			return rItem.m_nValue == m_nValue;
		}

	private:
		int m_nValue;
	};

	it = ring.FindIf(IsEqual(2));
	bool bSet = it.IsSet();
	CPPUNIT_ASSERT(bSet);
	int nValue = it->m_nValue;
	CPPUNIT_ASSERT_EQUAL(2, nValue);

	it = ring.FindIf(IsEqual(-1));
	bSet = it.IsSet();
	CPPUNIT_ASSERT(!bSet);
}
