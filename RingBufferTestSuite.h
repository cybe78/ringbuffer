#ifndef RING_BUFFER_TEST_SUITE
#define RING_BUFFER_TEST_SUITE

#include <cppunit/extensions/HelperMacros.h>

class CRingBufferTestSuite: public CppUnit::TestFixture
{
public:
	CPPUNIT_TEST_SUITE(CRingBufferTestSuite);
	CPPUNIT_TEST(TestSize);
	CPPUNIT_TEST(TestInit);
	CPPUNIT_TEST(TestIterators);
	CPPUNIT_TEST(TestConstDereferencing);
	CPPUNIT_TEST(TestDereferencing);
	CPPUNIT_TEST(TestOverwriting);
	CPPUNIT_TEST(TestCopying);
	CPPUNIT_TEST(TestPostIncrement);
	CPPUNIT_TEST(TestIsSet);
	CPPUNIT_TEST(TestPlus);
	CPPUNIT_TEST(TestPlus2);
	CPPUNIT_TEST(TestFindIf);
	CPPUNIT_TEST_SUITE_END();

public:
	// Tests

	void TestSize();
	void TestInit();
	void TestIterators();
	void TestConstDereferencing();
	void TestDereferencing();
	void TestOverwriting();
	void TestCopying();
	void TestPostIncrement();
	void TestIsSet();
	void TestPlus();
	void TestPlus2();
	void TestFindIf();
};

#endif //RING_BUFFER_TEST_SUITE